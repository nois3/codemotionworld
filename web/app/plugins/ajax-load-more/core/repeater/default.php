<div class="large-3 medium-6 columns single-new end">
  <div class="error-hide"><?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?></div>
  <div class="smallest clearfix">
    <div class="content">
      <span class="double-line center"></span>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="background-opacity"></a>
      <span class="image panel" style="background-image: url('<?php echo $url; ?>');">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="icon-discover"></span></a>
      </span>
      <span class="text-container">
        <h4 class="date"><?php the_time('F j, Y'); ?></h4>
        <h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
      </span>
    </div>
  </div>
</div>