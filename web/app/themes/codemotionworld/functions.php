<?php

include_once "cpt.php";
include_once "acf.php";

add_theme_support( 'post-thumbnails' );
//add_image_size('home-thumb', 578, 380 );

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( 
  array(
    'primary'   => __( 'Primary menu', 'codemotionworld' ),
    'internal'   => __( 'Primary menu internal', 'codemotionworld' ),
    'social' => __( 'Social links', 'codemotionworld' ),
    'mobile' => __( 'Primary menu mobile', 'codemotionworld' ),
  ) 
);

function codemotionworld_font_url() {
  $font_url = '';
  /*
   * Translators: If there are characters in your language that are not supported
   * by Lato, translate this to 'off'. Do not translate into your own language.
   */
  if ( 'off' !== _x( 'on', 'Raleway font: on or off', 'codemotionworld' ) ) {
    $font_url = add_query_arg( 'family', urlencode( 'Raleway:500,300,800' ), "//fonts.googleapis.com/css" );
  }

  return $font_url;
}

function codemotionworld_scripts() {
  // Load Font
  wp_enqueue_style( 'codemotionworld-raleway', codemotionworld_font_url(), array(), null );
  // Load our main stylesheet.
  wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css', array() );
  wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css', array() );
  wp_enqueue_style( 'roller', get_template_directory_uri() . '/css/fs.roller.css', array() );
  wp_enqueue_style( 'icon', get_template_directory_uri() . '/css/codemotion.css', array() );
  wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array() );

  // Load js
  wp_enqueue_script( 'codemotionworld-jquery', get_template_directory_uri() . '/js/vendor/jquery.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-modernizr', get_template_directory_uri() . '/js/vendor/modernizr.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-pace', get_template_directory_uri() . '/js/pace.min.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-sticky', get_template_directory_uri() . '/js/sticky.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-roller', get_template_directory_uri() . '/js/fs.roller.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-typer', get_template_directory_uri() . '/js/typer.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-share', get_template_directory_uri() . '/js/share.js', array(), '20140730', true );
  wp_enqueue_script( 'codemotionworld-script', get_template_directory_uri() . '/js/script.js', array(), '20140730', true );

}
// Qui richiamiamo la funzione degli script
add_action( 'wp_enqueue_scripts', 'codemotionworld_scripts' );

?>