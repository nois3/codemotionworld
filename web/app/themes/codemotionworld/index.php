<?php get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('section/section', 'hero'); ?>
<?php get_template_part('partials/head', 'sticky'); ?>
<?php wp_reset_postdata(); ?>
<?php get_template_part('section/section', 'map'); ?>
<?php get_template_part('section/section', 'about'); ?>
<?php get_template_part('section/section', 'news'); ?>
<?php get_template_part('section/section', 'speakers'); ?>
<?php get_template_part('section/section', 'services'); ?>
<?php get_template_part('section/section', 'communities'); ?>
<?php get_template_part('section/section', 'partners'); ?>
<?php get_template_part('section/section', 'contact'); ?>

<?php get_footer(); ?>