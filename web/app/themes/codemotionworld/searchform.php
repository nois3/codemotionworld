<div class="search-form">
  <form role="search" method="get" id="searchform" autocomplete="off" action="<?php echo home_url( '/' ); ?>">
    <label class="screen-reader-text" for="s">Search anytime by typing:</label>
    <input type="text" value="" name="s" id="s" placeholder="Search Here."/>
    <input type="submit" id="searchsubmit" value="Cerca" />
  </form>
  <div class="close-search-form"></div>
</div>