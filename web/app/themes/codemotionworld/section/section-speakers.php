<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'our-beloved-speakers'
  );
  $page_speakers = new WP_Query( $args );
  // The Loop
  if ( $page_speakers->have_posts() ):
    while ( $page_speakers->have_posts() ):
      $page_speakers->the_post();
?>

<section id="speakers" class="speakers">
  <div class="row collapse">
    <div class="large-6 medium-5 columns">
      <h2 class="title section brace-left"><?php the_title(); ?></h2>
      <span class="double-line big"></span>  
      <a href="<?php the_field('link_become_a_speaker', 'option'); ?>" target="_blank" class="button">
        <span>Become a Speaker</span>
      </a>   
    </div>
    <div class="large-6 medium-7 columns">
      <p class="big">
        <?php echo get_the_content(); ?>
      </p>
    </div>
  </div>
  <div class="row collapse target">

    <?php
      $args = array(
        'post_type' => 'speakers',
        'posts_per_page' => -1
      );
      $speakers = new WP_Query( $args );
      // The Loop
      if ( $speakers->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $speakers->set('orderby', 'menu_order');
        $speakers->set('order', 'ASC');
        $speakers->get_posts();
        while ( $speakers->have_posts() ):
          $speakers->the_post();
    ?>

    <div class="roller-item square">
      <span class="single-speaker panel square" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
        <span class="background-opacity"></span>
        <div class="content">
          <h2 class="name"><?php the_title(); ?></h2>
          <h4 class="job"><?php the_field('job_title'); ?></h4>
          <span class="double-line center"></span>
          <h4><?php the_field('company'); ?></h4>
        </div>
      </span>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>

  </div>
  <div class="row collapse">

    <div class="large-3 large-centered medium-3 medium-centered small-6 small-centered columns">
      <a href="<?php echo esc_url( home_url( '' ) ); ?>/speakers" class="see-all-speakers"><span>See All</span></a>
    </div>
     <!--<div class="large-4 columns">
      <h4 class="right"><a href="#" class="archive">See More<span class="icon-arrow-right right"></span></a></h4>
    </div> -->
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>