<?php wp_reset_postdata(); ?>

<section id="map" class="map">
  <div class="container-map hide-for-medium hide-for-small">

    <?php
      $args = array (
          'post_type' => 'event',
          'posts_per_page' => -1
      );
      $point_events = new WP_Query($args);
      if($point_events->have_posts()): 
        while($point_events->have_posts()):
          $point_events->the_post();
    ?>

    <div class="single-pin <?php the_field('event_position_map'); ?>" id="<?php echo( basename(get_permalink()) ); ?>" style="top: <?php the_field('position_point_y'); ?>%; left: <?php the_field('position_point_x'); ?>%">
      <div class="orange-pin">
        <div class="hover-pin"></div>
        <div class="container-event">
          <div class="close-event"></div>
          <h2 class="venue"><a href="<?php the_field('event_link'); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
          <span class="double-line big"></span>
          <?php $date_start = DateTime::createFromFormat('Ymd', get_field('date_start')); ?>
          <?php if( get_field('date_end') ): ?>
            <?php $date_end = DateTime::createFromFormat('Ymd', get_field('date_end')); ?>
            <h4 class="date"><?php echo $date_start->format('d, m'); ?><span class="icon-arrow-right-big"></span><?php echo $date_end->format('d, m, Y'); ?></h4>
          <?php else: ?>
            <h4 class="date"><?php echo $date_start->format('d, m, Y'); ?></h4>
          <?php endif; ?>
          <div class="event-content">
          <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>

    <?php
        endwhile;
      endif;
      wp_reset_postdata();
    ?>
  </div>

  <?php
    $args = array(
      'post_type' => 'page',
      'posts_per_page' => 1,
      'name' => 'map'
    );
    $about_page = new WP_Query( $args );
    // The Loop
    if ( $about_page->have_posts() ):
      while ( $about_page->have_posts() ):
        $about_page->the_post();
  ?>

  <div class="row collapse">
    <div class="large-3 large-offset-9 medium-4 medium-offset-8 columns">
      <div class="cfp">
        <span class="double-line big"></span>
        <h2 class="title small"><?php echo get_the_title(); ?></h2>
        <a href="<?php the_field('discover_how_link', 'option'); ?>" class="button">
          <span><?php the_field('discover_how_text', 'option'); ?></span>
        </a>
      </div>
    </div>
    <div class="map-mobile small-12 columns hide-for-large">
      <img src="<?php the_field('image_map_mobile', 'option'); ?>" alt="Event Map Mobile">
    </div>
    <div class="events-mobile hide-for-large">
      <div class="row collapse">
        <?php get_template_part('partials/event', 'map'); ?>
        <div class="small-12 columns archive-mobile">
          <h4><a href="<?php echo esc_url( home_url( '' ) ); ?>/events-archive/" class="archive">Events Archive</a></h4>
        </div>
      </div>
    </div>
  </div>
  <div class="map bottom hide-for-medium hide-for-small">
    <!-- / events / -->
    <div class="row collapse">
      <div class="events clearfix target">
        <?php get_template_part('partials/event', 'map'); ?>          
      </div>
    </div>
    <!-- / bottom / -->
    <div class="row collapse">
      <div class="large-4 large-offset-4 medium-4 medium-offset-4 columns">
        <a href="#about-us" class="discover clearfix scroll-js">
          <span class="icon-arrow-down-big"></span>
        </a>
      </div>
      <div class="large-4 medium-4 columns">
         <h4 class="right"><a href="<?php echo esc_url( home_url( '' ) ); ?>/events-archive/" class="archive">Events Archive</a></h4>
      </div>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>