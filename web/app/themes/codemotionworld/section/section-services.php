<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'our-services'
  );
  $page_services = new WP_Query( $args );
  // The Loop
  if ( $page_services->have_posts() ):
    while ( $page_services->have_posts() ):
      $page_services->the_post();
?>

<section id="ourservices" class="ourservices">
  <div class="row collapse">
    <div class="container-centered">
      <div class="large-12 medium-12 small-12 columns">
        <h2 class="title section"><?php echo get_the_content(); ?></h2>
        <span class="double-line big center"></span>
        <h3><?php the_title(); ?></h3>
      </div>

      <?php
        $args = array(
          'post_type' => 'services',
          'posts_per_page' => -1
        );
        $services = new WP_Query( $args );
        // The Loop
        if ( $services->have_posts() ):
          // REORDER ACCORDING TO METRONET
          $services->set('orderby', 'menu_order');
          $services->set('order', 'ASC');
          $services->get_posts();
          while ( $services->have_posts() ):
            $services->the_post();
      ?>

      <div class="large-2 medium-4 small-6 columns end">
        <div class="single-service">
          <a href="<?php echo esc_url( home_url( '' ) ); ?>/our-services" title="<?php the_title(); ?>" class="icon-service"><?php the_post_thumbnail(); ?></a>
          <h4><?php the_title(); ?></h4>
        </div>
      </div>

      <?php 
        endwhile;
        endif;
        wp_reset_query();
        wp_reset_postdata();
      ?>

    </div>
  </div>
  <div class="row collapse">
  <div class="large-4 medium-6 small-10 columns large-centered medium-centered small-centered">
      <a href="<?php the_field('button_services_link', 'option'); ?>" class="button">
        <span class="disappear-580"><?php the_field('button_services_text', 'option'); ?></span>
        <span class="appear-580">WHAT WE DO IN ITALY</span>
      </a>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>