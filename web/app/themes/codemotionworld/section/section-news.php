<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'home-news'
  );
  $page_news = new WP_Query( $args );
  // The Loop
  if ( $page_news->have_posts() ):
    while ( $page_news->have_posts() ):
      $page_news->the_post();
?>

<section id="news" class="news">
  <div class="row collapse">
    <div class="large-12 medium-12 small-12 columns">
      <div class="news-container">
        <h3><?php the_title(); ?></h3>
        <span class="double-line center"></span>
      </div>
    </div>

    <?php
      $sticky = get_option( 'sticky_posts' );
      $args = array(
        'posts_per_page' => 1,
        'post__in'  => $sticky,
        'ignore_sticky_posts' => 1
      );
      $sticky_post = new WP_Query( $args );
      if ( $sticky[0] ):
      // The Loop
        if ( $sticky_post->have_posts() ):
          while ( $sticky_post->have_posts() ):
            $sticky_post->the_post();
    ?>

    <div class="large-6 medium-6 small-12 columns single-new-big">
      <div class="main panel square" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
        <div class="background-opacity"></div>
        <div class="background-gradient"></div>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="text-container">
          <span class="icon-discover"></span>
          <div class="content">
            <h4 class="date"><?php the_time('F j, Y'); ?></h4>
            <h2 class="title"><?php the_title(); ?></h2>
            <span class="double-line big center"></span>
          </div>
        </a>
      </div>
    </div>

    <?php
      endwhile;
      endif;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>

    <?php
      $args_no_sticky = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'ignore_sticky_posts' => 1,
        'post__not_in' => get_option( 'sticky_posts' )
      );
      $blog = new WP_Query( $args_no_sticky );
      // The Loop
      if ( $blog->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $blog->set('orderby', 'menu_order');
        $blog->set('order', 'ASC');
        $blog->get_posts();
        $count = 1;
        while ( $blog->have_posts() ):
          $blog->the_post();
    ?>

    <div class="large-6 medium-6 small-12 columns single-new number-<?php echo $count ?>">
      <div class="latest clearfix">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="background-opacity"></a>
        <span class="image panel square" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="icon-discover"></span></a>
        </span>
        <span class="text-container square">
          <div class="content">
            <h4 class="date"><?php the_time('F j, Y'); ?></h4>
            <h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
            <span class="double-line"></span>
            <div class="excerpt">
              <?php the_excerpt(); ?>
            </div>
          </div>
        </span>
      </div>
    </div>

    <?php 
      $count++;
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>

    <?php get_template_part('partials/news', 'video'); ?>
    
  </div>
  <div class="row collapse">
    <div class="large-12 medium-12 small-12 columns">
      <a href="<?php echo esc_url( home_url( '' ) ); ?>/news" class="button"><span>News Archive</span></a>
    </div>
  </div>
</section>

<?php
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>