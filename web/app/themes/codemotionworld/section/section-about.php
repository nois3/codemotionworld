<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'aboutus'
  );
  $aboutus_page = new WP_Query( $args );
  // The Loop
  if ( $aboutus_page->have_posts() ):
    while ( $aboutus_page->have_posts() ):
      $aboutus_page->the_post();
?>

<section id="about-us" class="about-us">
  <div class="row collapse">
    <div class="large-4 medium-12 small-12 columns hide-for-medium">
      <div class="people-container clearfix">
        <?php $child_pages = get_pages('child_of=' . $post->ID . '&sort_column=menu_order'); ?>
          <?php foreach ($child_pages as $page): ?>
          <div class="large-12 medium-4 columns small-12 people">
            <div class="row collapse">
              <div class="large-3 medium-12 small-2 columns">
                <a href="<?php echo get_field('link_section', $page->ID); ?>" title="<?php echo $page->post_title; ?>" class="about-icon square"><?php echo get_the_post_thumbnail($page->ID); ?></a>
              </div>
              <div class="large-9 medium-12 small-10 columns">
                <h4><?php echo $page->post_title; ?></h4>
                <span class="double-line"></span>   
                <p><?php echo $page->post_content; ?></p>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
      </div>
    </div>
    <div class="large-7 large-offset-1 medium-12 columns">
      <div class="about-container">
        <h2 class="title section brace-left"><?php echo get_the_title(); ?></h2>
        <span class="double-line big"></span>
        <p class="big"> 
          <?php echo get_the_content(); ?>
        </p>
        <p><a href="<?php echo esc_url( home_url( '' ) ); ?>/about/" class="read-more"><span class="icon-arrow-right"></span>Read More</a></p>
      </div>
    </div>
    <div class="large-4 medium-12 small-12 columnss show-for-medium">
      <div class="people-container clearfix">
        <?php $child_pages = get_pages('child_of=' . $post->ID . '&sort_column=menu_order'); ?>
          <?php foreach ($child_pages as $page): ?>
          <div class="large-12 medium-4 columns small-12 people">
            <div class="row collapse">
              <div class="large-3 medium-12 small-2 columns">
                <a href="<?php echo get_field('link_section', $page->ID); ?>" title="<?php echo $page->post_title; ?>" class="about-icon square"><?php echo get_the_post_thumbnail($page->ID); ?></a>
              </div>
              <div class="large-9 medium-12 small-10 columns">
                <h4><?php echo $page->post_title; ?></h4>
                <span class="double-line"></span>   
                <p><?php echo $page->post_content; ?></p>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>