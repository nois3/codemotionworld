<!-- / HERO / -->
<section id="hero" class="hero" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');"> 
  <div class="hero header">
    <!-- / events / -->
    <div class="row collapse">
      <div class="events clearfix target">
        <?php get_template_part('partials/home', 'hero'); ?>
      </div>
    </div>
    <!-- / topbar / -->
    <div class="hero topbar">
      <div class="row collapse">
        <div class="large-10 medium-9 columns">
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'menu clearfix' ) ); ?>
          <span class="icon-search search"></span>
        </div>
        <div class="large-2 medium-3 columns">
          <h4 class="right"><a href="<?php echo esc_url( home_url( ' / ' ) ); ?>events-archive/" class="archive">Events Archive</a></h4>
        </div>
      </div>
    </div>
  </div>
  <!-- / main / -->
  <div class="hero main">
    <div class="row collapse">
      <h1 class="logo clearfix"><?php the_title(); ?></h1>
      <h2 class="headline"><?php echo get_the_content(); ?></h2>

      <h2 class="disappear-580">More Than <span data-typer-targets="

      <?php 
        $rows = get_field('more_than', 'option');
        if($rows) {
          foreach($rows as $row) {
            echo $row['more_than_text'] . ',';
          }
        }
      ?>

      " class="change"></span></h2>
      
      <span class="double-line big"></span>
      <a href="<?php the_field('link_become_a_speaker', 'option'); ?>" target="_blank" class="button">
        <span>Become a Speaker</span>
      </a>
    </div>
  </div>
  <!-- / bottom / -->
  <div class="hero bottom">
    <div class="row collapse">
      <div class="large-5 medium-5 small-5 columns">
        <h4 class="clearfix"><a href="#contact" class="contact-us scroll-js">Contact us</a></h4>
        <h4 class="clearfix"><a href="<?php the_field('link_newsletter', 'option'); ?>" target="_blank"><span class="icon-mail"></span>Suscribe Newsletter</a></h4>
      </div>
      <div class="large-2 medium-2 small-2 columns">
        <a href="#map" class="discover clearfix scroll-js">
          <span class="icon-arrow-down-big"></span>
        </a>
      </div>
      <div class="large-5 medium-5 small-5 columns">
        <div class="social right">
          <?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu-social clearfix' ) ); ?>
        </div>
      </div>
    </div>
  </div>
</section>