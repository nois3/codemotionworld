<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'our-communities'
  );
  $page_communities = new WP_Query( $args );
  // The Loop
  if ( $page_communities->have_posts() ):
    while ( $page_communities->have_posts() ):
      $page_communities->the_post();
?>

<section id="ourcommunities" class="ourcommunities">
  <div class="row collapse">
    <div class="large-12 columns">
      <div class="ourcommunities-container">
        <h3><?php the_title(); ?></h3>
        <span class="double-line center"></span>
      </div>
    </div>
  </div>
  <div class="row collapse target">

    <?php
      $args = array(
        'post_type' => 'communities',
        'posts_per_page' => -1
      );
      $communities = new WP_Query( $args );
      // The Loop
      if ( $communities->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $communities->set('orderby', 'menu_order');
        $communities->set('order', 'ASC');
        $communities->get_posts();
        while ( $communities->have_posts() ):
          $communities->the_post();
    ?>

    <div class="roller-item square">
      <a href="<?php the_field('link_site'); ?>" target="_blank" class="single-community square">
        <?php the_post_thumbnail(); ?>
      </a>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>

  </div>
  <div class="row">
    <div class="large-12 medium-6 medium-centered columns">
      <h4><a href="<?php echo esc_url( home_url( '' ) ); ?>/communities">See All</a></h4>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>