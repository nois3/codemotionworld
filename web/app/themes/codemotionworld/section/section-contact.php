<!-- / CONTACT / -->

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'contact'
  );
  $about_page = new WP_Query( $args );
  // The Loop
  if ( $about_page->have_posts() ):
    while ( $about_page->have_posts() ):
      $about_page->the_post();
?>

<section id="contact" class="contact">
  <div class="row collapse">
    <div class="large-12 columns">
      <div class="contact-container">
        <h3><?php the_title(); ?></h3>
        <span class="double-line center"></span>
      </div>
    </div>
    <div class="large-4 large-offset-4 medium-8 medium-offset-2 small-12 end columns">
      <div class="container-form clearfix">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>