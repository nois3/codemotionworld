<?php wp_reset_postdata(); ?>

<?php
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 1,
    'name' => 'partners'
  );
  $page_partners = new WP_Query( $args );
  // The Loop
  if ( $page_partners->have_posts() ):
    while ( $page_partners->have_posts() ):
      $page_partners->the_post();
?>

<section id="partners" class="partners">
  <div class="row collapse">
    <div class="large-12 columns">
      <div class="partners-container">
        <h3><?php the_title(); ?></h3>
        <span class="double-line center"></span>
      </div>
    </div>
  </div>
  <div class="row collapse target">

    <?php
      $args = array(
        'post_type' => 'partners',
        'posts_per_page' => -1
      );
      $partners = new WP_Query( $args );
      // The Loop
      if ( $partners->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $partners->set('orderby', 'menu_order');
        $partners->set('order', 'ASC');
        $partners->get_posts();
        while ( $partners->have_posts() ):
          $partners->the_post();
    ?>

    <div class="roller-item square">
      <a href="<?php the_field('link_site'); ?>" target="_blank" class="single-community square">
        <?php the_post_thumbnail(); ?>
      </a>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>
  </div>
  <div class="row">
    <div class="large-12 medium-6 small-6 medium-centered small-centered columns">
      <h4><a href="<?php echo esc_url( home_url( '' ) ); ?>/partners">See All</a></h4>
    </div>
  </div>
</section>

<?php 
  endwhile;
  endif;
  wp_reset_query();
  wp_reset_postdata();
?>

<?php wp_reset_postdata(); ?>