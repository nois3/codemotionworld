<?php get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!-- / NEWS / -->
<section id="article" class="article">
  <div class="row collapse">
    <div class="image-cover">
      <div class="image-internal"><?php the_post_thumbnail(); ?></div>
      <div class="background-opacity"></div>
      <div class="background-gradient"></div>
      <div class="main-container">
        <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
          <div class="title-container">
            <h2 class="title"><?php the_title(); ?></h2>
            <?php $subtitle = get_field('subtitle'); ?>
            <?php if($subtitle) { ?>
              <h3 class="subtitle"><?php the_field('subtitle'); ?></h3>
            <?php } ?>
            <span class="double-line big"></span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-side-fixed">
    <div class="row collapse">
      <div class="large-2 columns">
        <div class="container-back">
          <a href="<?php echo esc_url( home_url( '' ) ); ?>/news"><span class="icon-arrow-left-big"></span>Back</a>
        </div>
        <div id="socializzami"></div>
      </div>
    </div>
  </div>
  
  <div class="row article-background collapse">
    <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
      <div class="single-new">
        <div class="article-container">
          <h4 class="date">Posted on <strong><?php the_time('m, j, Y'); ?></strong></h4>
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row collapse newer-older ">
      <div class="large-4 medium-6 large-offset-2 columns">
      <?php 
        $p = get_adjacent_post(false, '', true);
        if(!empty($p)):
          echo '<a class="prev left" href="' . get_permalink($p->ID) . '" title="' . $p->post_title . '"><span class="icon-arrow-left-big"></span><span>prev article</span><span class="title">' . $p->post_title . '</span></a>';
        else:
          echo '&nbsp;';
        endif;
      ?>
      </div>
      <div class="large-4 medium-6 columns end">
      <?php
        $n = get_adjacent_post(false, '', false);
        if(!empty($n)):
          echo '<a class="next right" href="' . get_permalink($n->ID) . '" title="' . $n->post_title . '"><span class="icon-arrow-right-big"></span><span>next article</span><span class="title">' . $n->post_title . '</span></a>';
        else:
          echo '&nbsp;';
        endif;
      ?>
      </div>
    </div>
  </div>
</section>



<?php endwhile; /* fine del loop */ ?>

<?php get_footer(); ?>