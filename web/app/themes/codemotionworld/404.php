<?php get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / 404 / -->
<section id="error" class="error height-screen-js">
  <div class="background-opacity"></div>
  <div class="row collapse vertical-align-class">
    <div class="large-12 columns">
      <h2 class="title section">Error 404.</h2>
      <h3 class="subtitle">There is nothing to see here.</h3>
      <span class="double-line big"></span> 
      <a href="<?php echo esc_url( home_url( '' ) ); ?>" title="Back to home">Back to home</a>
    </div>
  </div>
</section>

<?php get_footer(); ?>