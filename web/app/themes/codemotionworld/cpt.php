<?php

add_action('init', 'cptui_register_my_cpt_video');
function cptui_register_my_cpt_video() {
register_post_type('video', array(
'label' => 'Video',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'video', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Video',
  'singular_name' => 'Video',
  'menu_name' => 'Video',
  'add_new' => 'Add Video',
  'add_new_item' => 'Add New Video',
  'edit' => 'Edit',
  'edit_item' => 'Edit Video',
  'new_item' => 'New Video',
  'view' => 'View Video',
  'view_item' => 'View Video',
  'search_items' => 'Search Video',
  'not_found' => 'No Video Found',
  'not_found_in_trash' => 'No Video Found in Trash',
  'parent' => 'Parent Video',
)
) ); }

add_action('init', 'cptui_register_my_cpt_services');
function cptui_register_my_cpt_services() {
register_post_type('services', array(
'label' => 'Services',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'services', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Services',
  'singular_name' => 'services',
  'menu_name' => 'Services',
  'add_new' => 'Add services',
  'add_new_item' => 'Add New services',
  'edit' => 'Edit',
  'edit_item' => 'Edit services',
  'new_item' => 'New services',
  'view' => 'View services',
  'view_item' => 'View services',
  'search_items' => 'Search Services',
  'not_found' => 'No Services Found',
  'not_found_in_trash' => 'No Services Found in Trash',
  'parent' => 'Parent services',
)
) ); }

// CREATING CONTENT TYPE FOR EVENTS
add_action('init', 'cptui_register_my_cpt_event');
function cptui_register_my_cpt_event() {
  register_post_type('event', array(
    'label' => 'Events',
    'description' => '',
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'map_meta_cap' => true,
    'hierarchical' => false,
    'rewrite' => array('slug' => 'event', 'with_front' => true),
    'query_var' => true,
    'has_archive' => true,
    'menu_position' => '10',
    'supports' => array('title','editor','excerpt','thumbnail','page-attributes','post-formats'),
    'taxonomies' => array('category'),
    'labels' => array (
        'name' => 'Events',
        'singular_name' => 'Event',
        'menu_name' => 'Events',
        'add_new' => 'Add Event',
        'add_new_item' => 'Add New Event',
        'edit' => 'Edit',
        'edit_item' => 'Edit Event',
        'new_item' => 'New Event',
        'view' => 'View Event',
        'view_item' => 'View Event',
        'search_items' => 'Search Events',
        'not_found' => 'No Events Found',
        'not_found_in_trash' => 'No Events Found in Trash',
        'parent' => 'Parent Event',
      )
    )
  ); 
}

// CREATING CONTENT TYPE FOR SPEAKERS
add_action('init', 'cptui_register_my_cpt_speakers');
function cptui_register_my_cpt_speakers() {
register_post_type('speakers', array(
'label' => 'Speakers',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'speakers', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Speakers',
  'singular_name' => 'Speaker',
  'menu_name' => 'Speakers',
  'add_new' => 'Add Speaker',
  'add_new_item' => 'Add New Speaker',
  'edit' => 'Edit',
  'edit_item' => 'Edit Speaker',
  'new_item' => 'New Speaker',
  'view' => 'View Speaker',
  'view_item' => 'View Speaker',
  'search_items' => 'Search Speakers',
  'not_found' => 'No Speakers Found',
  'not_found_in_trash' => 'No Speakers Found in Trash',
  'parent' => 'Parent Speaker',
)
) ); }

// CREATING CONTENT TYPE FOR TEAM
add_action('init', 'cptui_register_my_cpt_team');
function cptui_register_my_cpt_team() {
register_post_type('team', array(
'label' => 'Team',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'team', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Team',
  'singular_name' => 'Team',
  'menu_name' => 'Team',
  'add_new' => 'Add Team',
  'add_new_item' => 'Add New Team',
  'edit' => 'Edit',
  'edit_item' => 'Edit Team',
  'new_item' => 'New Team',
  'view' => 'View Team',
  'view_item' => 'View Team',
  'search_items' => 'Search Team',
  'not_found' => 'No Team Found',
  'not_found_in_trash' => 'No Team Found in Trash',
  'parent' => 'Parent Team',
)
) ); }

// CREATING CONTENT TYPE FOR COMMUNITIES
add_action('init', 'cptui_register_my_cpt_communities');
function cptui_register_my_cpt_communities() {
register_post_type('communities', array(
'label' => 'Communities',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'communities', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Communities',
  'singular_name' => 'Communities',
  'menu_name' => 'Communities',
  'add_new' => 'Add Communities',
  'add_new_item' => 'Add New Communities',
  'edit' => 'Edit',
  'edit_item' => 'Edit Communities',
  'new_item' => 'New Communities',
  'view' => 'View Communities',
  'view_item' => 'View Communities',
  'search_items' => 'Search Communities',
  'not_found' => 'No Communities Found',
  'not_found_in_trash' => 'No Communities Found in Trash',
  'parent' => 'Parent Communities',
)
) ); }

// CREATING CONTENT TYPE FOR PARTNERS
add_action('init', 'cptui_register_my_cpt_partners');
function cptui_register_my_cpt_partners() {
register_post_type('partners', array(
'label' => 'Partners',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'partners', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Partners',
  'singular_name' => 'Partners',
  'menu_name' => 'Partners',
  'add_new' => 'Add Partners',
  'add_new_item' => 'Add New Partners',
  'edit' => 'Edit',
  'edit_item' => 'Edit Partners',
  'new_item' => 'New Partners',
  'view' => 'View Partners',
  'view_item' => 'View Partners',
  'search_items' => 'Search Partners',
  'not_found' => 'No Partners Found',
  'not_found_in_trash' => 'No Partners Found in Trash',
  'parent' => 'Parent Partners',
)
) ); }

// CREATING CONTENT TYPE FOR TIMELINE
// add_action('init', 'cptui_register_my_cpt_timeline');
// function cptui_register_my_cpt_timeline() {
//   register_post_type('timeline', array(
//     'label' => 'Timelines',
//     'description' => '',
//     'public' => true,
//     'show_ui' => true,
//     'show_in_menu' => true,
//     'capability_type' => 'post',
//     'map_meta_cap' => true,
//     'hierarchical' => false,
//     'rewrite' => array('slug' => 'timeline', 'with_front' => true),
//     'query_var' => true,
//     'menu_position' => '15',
//     'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
//     'labels' => array (
//         'name' => 'Timelines',
//         'singular_name' => 'Timeline',
//         'menu_name' => 'Timelines',
//         'add_new' => 'Add Timeline',
//         'add_new_item' => 'Add New Timeline',
//         'edit' => 'Edit',
//         'edit_item' => 'Edit Timeline',
//         'new_item' => 'New Timeline',
//         'view' => 'View Timeline',
//         'view_item' => 'View Timeline',
//         'search_items' => 'Search Timelines',
//         'not_found' => 'No Timelines Found',
//         'not_found_in_trash' => 'No Timelines Found in Trash',
//         'parent' => 'Parent Timeline',
//       )
//     ) 
//   ); 
// }

?>