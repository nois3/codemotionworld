<?php
/*
 * Template Name: Speakers
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / ABOUT / -->
<section id="speakers-page" class="speakers-page">
  <div class="row collapse">
    <div class="large-12 medium-10 medium-offset-1 large-offset-0 columns end">
      <h2 class="title section"><?php the_title(); ?></h2>
      <span class="double-line big"></span> 
    </div>
  </div>
  <div class="row collapse">
    <div class="large-4 medium-10 medium-offset-1 large-offset-0 columns end">  
      <h2 class="title serif">
        <?php echo get_field('subtitle'); ?>
      </h2>
      <a href="<?php the_field('link_become_a_speaker', 'option'); ?>" target="_blank" class="button">
        <span>Become a Speaker</span>
      </a>
    </div>
    <div class="large-8 medium-10 medium-offset-1 large-offset-0 columns end">  
      <p class="big"><?php echo get_the_content(); ?></p>
    </div>
  </div>
  
  <div class="row collapse">
    <?php
      $args = array(
        'post_type' => 'speakers',
        'posts_per_page' => -1
      );
      $speakers = new WP_Query( $args );
      // The Loop
      if ( $speakers->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $speakers->set('orderby', 'menu_order');
        $speakers->set('order', 'ASC');
        $speakers->get_posts();
        while ( $speakers->have_posts() ):
          $speakers->the_post();
    ?>

    <div class="large-3 medium-4 small-12 columns end single" id="<?php echo( basename(get_permalink()) ); ?>" data-single="<?php echo( basename(get_permalink()) ); ?>">
      <div class="smallest clearfix">
        <div class="content">
          <span class="background-opacity"></span>
          <span class="image panel" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
            <span class="icon-discover"></span>
          </span>
          <span class="text-container">
            <h4 class="job-title"><?php the_field('job_title'); ?></h4>
            <h2 class="title"><?php the_title(); ?></h2>
            <span class="double-line center"></span>
            <h3 class="company"><?php the_field('company'); ?></h3>
          </span>
        </div>
      </div>

      <div class="modal">
        <div class="top">
          <div class="close-modal"></div>
          <div class="background-opacity"></div>
          <div class="image" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>')"></div>
          <div class="arrow-left"><span class="icon-arrow-left-big"></span></div>
          <div class="arrow-right"><span class="icon-arrow-right-big"></span></div>
          <div class="container-text">
            <div class="job-title"><?php the_field('job_title'); ?></div>
            <div class="title"><?php the_title(); ?></div>
            <span class="double-line big center"></span>
            <div class="company"><?php the_field('company'); ?></div>
          </div>
        </div>
        <div class="content-info">
          <div class="title">About this speaker</div>
          <div class="body"><?php the_content(); ?></div>
          <div class="social-web clearfix">
            <div class="web">
              <div class="title">Website</div>
              <a href="<?php the_field('website'); ?>" target="_blank" class="site"><?php the_field('website'); ?></a>
            </div>
            <div class="social clearfix">
              <div class="title">Social</div>
              <a href="<?php the_field('twitter'); ?>" target="_blank" class="twitter"><span class="icon-twitter"></span>Twitter</a>
              <a href="<?php the_field('linkedin'); ?>" target="_blank" class="linkedin"><span class="icon-linkedin"></span>Linkedin</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>
  </div>

  <div class="overlay-modal"></div>
</section>

<?php get_footer(); ?>