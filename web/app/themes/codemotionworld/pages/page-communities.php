<?php
/*
 * Template Name: Communities
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / COMMUNITIES / -->
<section id="communities" class="communities">
  <div class="row collapse">
    <div class="large-12 columns">
      <h2 class="title section"><?php the_title(); ?></h2>
      <span class="double-line big"></span> 
    </div>
  </div>
  <div class="row collapse">
    <div class="large-4 columns">  
      <h2 class="title serif">
        <?php echo get_field('subtitle'); ?>
      </h2>
    </div>
    <div class="large-8 columns">  
      <p class="big"><?php echo get_the_content(); ?></p>
    </div>
  </div>
  <div class="row collapse">

    <?php
      $args = array(
        'post_type' => 'communities',
        'posts_per_page' => -1
      );
      $communities = new WP_Query( $args );
      // The Loop
      if ( $communities->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $communities->set('orderby', 'menu_order');
        $communities->set('order', 'ASC');
        $communities->get_posts();
        while ( $communities->have_posts() ):
          $communities->the_post();
    ?>

    <div class="large-2 medium-3 small-6 columns end">
      <a href="<?php the_field('link_site'); ?>" target="_blank">
        <div class="single-community-container">
          <div class="single-community square">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="community-text">
            <span class="title"><?php the_title(); ?></span>
            <span class="icon-discover"></span>
          </div>
        </div>
      </a>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>

  </div>

</section>

<?php get_footer(); ?>