<?php
/*
 * Template Name: Team
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / ABOUT / -->
<section id="team-page" class="team-page">
  <div class="row collapse">
    <div class="large-12 medium-10 medium-offset-1 large-offset-0 columns end">
      <h2 class="title section"><?php the_title(); ?></h2>
      <span class="double-line big"></span> 
    </div>
  </div>
  <div class="row collapse">
    <div class="large-4 medium-10 medium-offset-1 large-offset-0 columns end">  
      <h2 class="title serif">
        <?php echo get_field('subtitle'); ?>
      </h2>
    </div>
    <div class="large-8 medium-10 medium-offset-1 large-offset-0 columns end">  
      <p class="big"><?php echo get_the_content(); ?></p>
    </div>
  </div>
  
  <div class="row collapse">
    <?php
      $args = array(
        'post_type' => 'team',
        'posts_per_page' => -1
      );
      $team = new WP_Query( $args );
      // The Loop
      if ( $team->have_posts() ):
        // REORDER ACCORDING TO METRONET
        $team->set('orderby', 'menu_order');
        $team->set('order', 'ASC');
        $team->get_posts();
        while ( $team->have_posts() ):
          $team->the_post();
    ?>

    <div class="large-3 medium-4 small-12 columns end single" id="<?php echo( basename(get_permalink()) ); ?>" data-single="<?php echo( basename(get_permalink()) ); ?>">
      <div class="smallest clearfix">
        <div class="content">
          <span class="background-opacity"></span>
          <span class="image panel" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
            <span class="icon-discover"></span>
          </span>
          <span class="text-container">
            <h2 class="title"><?php the_title(); ?></h2>
            <span class="double-line center"></span>
            <h4 class="job-title"><?php the_field('job_title'); ?></h4>
          </span>
        </div>
      </div>

      <div class="modal">
        <div class="top">
          <div class="close-modal"></div>
          <div class="background-opacity"></div>
          <div class="image" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>')"></div>
          <div class="arrow-left"><span class="icon-arrow-left-big"></span></div>
          <div class="arrow-right"><span class="icon-arrow-right-big"></span></div>
          <div class="container-text">
            <div class="title"><?php the_title(); ?></div>
            <span class="double-line big center"></span>
            <div class="job-title"><?php the_field('job_title'); ?></div>
          </div>
        </div>
        <div class="content-info">
          <div class="title">About this Team Member</div>
          <div class="body"><?php the_content(); ?></div>
          <div class="social-web clearfix">
            <div class="social clearfix">
              <div class="title">Social</div>
              <a href="<?php the_field('twitter'); ?>" target="_blank" class="twitter"><span class="icon-twitter"></span>Twitter</a>
              <a href="<?php the_field('linkedin'); ?>" target="_blank" class="linkedin"><span class="icon-linkedin"></span>Linkedin</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php 
      endwhile;
      endif;
      wp_reset_query();
      wp_reset_postdata();
    ?>
  </div>

  <div class="overlay-modal"></div>
</section>

<?php get_footer(); ?>