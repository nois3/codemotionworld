<?php
/*
 * Template Name: Services
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / SERVICES / -->
<section id="services" class="services">
  <div class="row collapse">
    <div class="container-centered clearfix">
      <div class="large-12 columns">
        <h3><?php the_title(); ?></h3>
        <span class="double-line big center"></span>
        <h2 class="title section"><?php echo get_the_content(); ?></h2>
      </div>
    </div>
  </div>
  <div class="row collapse">
    <div class="large-4 large-centered medium-6 medium-centered columns">
      <a href="<?php the_field('link_become_a_speaker', 'option'); ?>" target="_blank" class="button">
        <span>Disclose What We Do in Italy</span>
      </a>
    </div>
  </div>
  <div class="services-container">
    <div class="row collapse">
      <?php
        $args = array(
          'post_type' => 'services',
          'posts_per_page' => -1
        );
        $services = new WP_Query( $args );
        // The Loop
        if ( $services->have_posts() ):
          // REORDER ACCORDING TO METRONET
          $services->set('orderby', 'menu_order');
          $services->set('order', 'ASC');
          $services->get_posts();
          while ( $services->have_posts() ):
            $services->the_post();
      ?>

      <div class="large-4 medium-6 small-12 columns end">
        <div class="single-service clearfix">
          <span class="icon-container">
            <span class="icon-service"><?php the_post_thumbnail(); ?></span>
          </span>
          <div class="services-text">
            <h4><?php the_title(); ?></h4>
            <span class="double-line"></span>
            <p><?php the_content(); ?></p>
            <a href="<?php the_field('external_link'); ?>" target="_blank">See More</a>
          </div>
        </div>
      </div>

      <?php 
        endwhile;
        endif;
        wp_reset_query();
        wp_reset_postdata();
      ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>