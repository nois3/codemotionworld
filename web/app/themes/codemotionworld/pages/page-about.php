<?php
/*
 * Template Name: About
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / ABOUT / -->
<section id="about" class="about">
  <div class="row collapse">
    <div class="large-12 medium-10 small-12 medium-centered columns">
      <h2 class="title section"><?php the_title(); ?></h2>
      <span class="double-line big"></span> 
    </div>
  </div>
  <div class="row collapse">
    <div class="large-4 medium-6 medium-offset-1 large-offset-0 columns">  
      <h2 class="title serif">
        <?php echo get_field('subtitle'); ?>
      </h2>
    </div>
    <div class="large-8 medium-10 medium-offset-1 large-offset-0 small-12 columns end">  
      <p class="big"><?php echo get_the_content(); ?></p>
    </div>
  </div>
  <div class="row collapse">
    <div class="image-cover">
      <div class="image-internal"><?php the_post_thumbnail(); ?></div>
    </div>
  </div>
  <div class="row collapse network-container">
    <div class="large-12 columns">
      <h3>Our Network</h3> 
      <h2 class="title serif">
        We are More Than:
      </h2>
    </div>
  </div>
  <div class="row collapse network-container">

    <?php
    if( have_rows('our_network') ):
        while ( have_rows('our_network') ) : the_row();
    ?>
      <div class="large-4 medium-4 small-6 columns end">
        <h2 class="value"><?php the_sub_field('number'); ?></h2> 
        <span class="double-line"></span>
        <h4 class="people">
          <?php the_sub_field('name'); ?>
        </h4>
      </div>
    <?php
        endwhile;
    endif;
    ?>
  </div>

</section>

<?php get_footer(); ?>