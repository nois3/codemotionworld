<?php
/*
 * Template Name: Events Archive
 */

get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / EVENT ARCHIVE / -->
<section id="events-archive" class="events-archive">
  <div class="row collapse">
    <div class="large-12 columns">
      <div class="title-container">
        <h1><?php the_title(); ?></h1>
        <span class="double-line center"></span>

      </div>
    </div>
    <div class="large-10 large-offset-1 medium-12 columns end">
      <div class="container-event-archive">
        <?php
        if( have_rows('single_event') ):
          while ( have_rows('single_event') ) : the_row();
        ?>
          <div class="row collapse container-single-event">
            <div class="large-2 medium-3 columns container-date square">
              <div class="year"><?php the_sub_field('year'); ?></div>
            </div>
            <div class="large-8 medium-9 columns container-body end">
              <h3 class="title"><?php the_sub_field('title'); ?></h3>
              <div class="day-mounth"><?php the_sub_field('day_and_month'); ?></div>
              <span class="double-line center"></span>
              <div class="body"><?php the_sub_field('body'); ?></div>
              <?php
              if( have_rows('image_gallery') ):
              ?>
              <div class="fotorama" data-width="100%" data-allowfullscreen="true">
              <?php
                while ( have_rows('image_gallery') ) : the_row();
              ?>
                <img src="<?php the_sub_field('single_image'); ?>" alt="<?php the_sub_field('title_image'); ?>">
              <?php
                endwhile;
              ?>
              </div>
              <?php
              endif;
              ?>

              <div class="fotorama" data-width="100%">
                <a href="<?php the_sub_field('link_video'); ?>"></a>
              </div>
            </div>

          </div>

        <?php
          endwhile;
        endif;
        ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>