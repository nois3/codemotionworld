<!-- l'unico e solo menu mobile -->
<div class="header-mobile show-768">
  <div class="open-nav">
    <div class="line"></div>
    <div class="line"></div>
    <div class="line"></div>
  </div>
</div>
<div class="search-mobile show-768">
  <span class="icon-search search"></span>
  <div class="close-search-form"></div>
</div>
<nav class="mobile clearfix show-768">
  <?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_class' => 'nav-menu' ) ); ?>
</nav>