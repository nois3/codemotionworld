<?php
  $args = array (
      'post_type' => 'video',
      'posts_per_page' => 1
  );
  $events = new WP_Query($args);
  if($events->have_posts()): 
    while($events->have_posts()):
      $events->the_post();
?>

<div class="large-6 medium-6 columns single-video">
  <div class="video panel">
    <div class="image-container">
      <span class="image square" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="background-opacity"></a>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="icon-play"></span></a>
      </span>
    </div>
    <span class="text-container square">
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="icon-discover"></span></a>
      <div class="content">
        <h4 class="date"><?php the_time('F j, Y'); ?></h4>
        <h2 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
      </div>
    </span>
  </div>
</div>

<?php
    endwhile;
  endif;
?>
<?php wp_reset_postdata(); ?>
