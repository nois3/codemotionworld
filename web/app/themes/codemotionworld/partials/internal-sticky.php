<header class="header">
  <div class="row collapse">
    <div class="topbar">
      <a href="<?php echo esc_url( home_url( ' / ' ) ); ?>" class="logo large-2 medium-2 columns panel"></a>
      <div class="large-3 medium-1 columns hide-for-medium">
        <div class="social">
          <?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu-social clearfix' ) ); ?>
        </div>
      </div>
      <div class="large-7 medium-10 columns">
        <span class="icon-search search"></span>
        <?php wp_nav_menu( array( 'theme_location' => 'internal', 'menu_class' => 'menu clearfix' ) ); ?>
      </div>
    </div>  
  </div>
</header>