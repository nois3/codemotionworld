<?php
  $args = array (
      'post_type' => 'event',
      'posts_per_page' => -1
  );
  $events = new WP_Query($args);
  if($events->have_posts()): 
    // REORDER ACCORDING TO METRONET
    $events->set('orderby', 'menu_order');
    $events->set('order', 'ASC');
    $events->get_posts();
    while($events->have_posts()):
      $events->the_post();
?>

<div class="large-3 medium-6 columns end roller-item">
  <?php
    foreach((get_the_category()) as $category) {
  ?>
  <div class="single-event <?php echo $category->slug;  ?>" data-hover="<?php echo( basename(get_permalink()) ); ?>">
    <h2 class="venue"><?php the_title(); ?></h2>
    <?php $date_start = DateTime::createFromFormat('Ymd', get_field('date_start')); ?>
    <?php if( get_field('date_end') ): ?>
      <?php $date_end = DateTime::createFromFormat('Ymd', get_field('date_end')); ?>
      <h4 class="date"><?php echo $date_start->format('d, m'); ?><span class="icon-arrow-right-big"></span><?php echo $date_end->format('d, m, Y'); ?></h4>
    <?php else: ?>
      <h4 class="date"><?php echo $date_start->format('d, m, Y'); ?></h4>
    <?php endif; ?>
    <h4 class="category">
    <?php
        echo $category->cat_name;
      }
    ?>
     </h4>
     <hr class="event-hover">
  </div>
</div>

<?php
    endwhile;
  endif;
?>
<?php wp_reset_postdata(); ?>
