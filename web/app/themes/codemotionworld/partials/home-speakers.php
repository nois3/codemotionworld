<?php
  $args = array (
      'post_type' => 'speaker',
      'posts_per_page' => -1
    );
  $speakers = new WP_Query($args);
  if($speakers->have_posts()): 
    while($speakers->have_posts()):
      $speakers->the_post();
?>
<?php
  the_title();
  the_post_thumbnail();
  the_excerpt();
?>
<?php
    endwhile;
  endif;
?>