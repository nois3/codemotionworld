<?php
  $args = array (
      'post_type' => 'service',
      'posts_per_page' => -1
    );
  $services = new WP_Query($args);
  if($services->have_posts()): 
    while($services->have_posts()):
      $services->the_post();
?>
<?php
  the_title();
  the_excerpt();
?>
<?php
    endwhile;
  endif;
?>