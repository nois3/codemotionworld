<?php get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<!-- / NEWS / -->
<section id="article" class="article video">

  <div class="container-side-fixed">
    <div class="row collapse">
      <div class="large-2 columns">
        <div class="container-back">
          <a href="<?php echo esc_url( home_url( '' ) ); ?>/news"><span class="icon-arrow-left-big"></span>Back</a>
        </div>
        <div id="socializzami"></div>
      </div>
    </div>
  </div>

  <div class="row article-background collapse">
    <div class="large-12 columns">
      <div class="fotorama"
           data-width="100%"
           data-ratio="16/9">
        <a href="<?php the_field('link_video'); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
      </div>
    </div>


      
    <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
      <div class="image-cover">
        <div class="main-container">
          <div class="title-container">
            <h2 class="title"><?php the_title(); ?></h2>
            <?php $subtitle = get_field('subtitle'); ?>
            <?php if($subtitle) { ?>
              <h3 class="subtitle"><?php the_field('subtitle'); ?></h3>
            <?php } ?>
            <span class="double-line big"></span>
          </div>
        </div>
      </div>
    </div>


    <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
      <div class="single-new">
        <div class="article-container">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>

</section>

<?php get_footer(); ?>