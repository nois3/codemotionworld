function calcSize() {

  var heightScreen = $(window).height();
  var widthScreen = $(window).width();
  var heightHeader = $("header.header").outerHeight();
  var heightHeroMain = $("#hero .hero.main").outerHeight();
  var widthMap = $(".container-map").outerWidth();
  var heightMap = $(".container-map").outerHeight();
  var mapRatio = (widthMap/12)*7;

  $("#hero").css({ "height": heightScreen });
  $(".height-screen-js").css({ "height": heightScreen-148 });
  // $("#map").css({ "height": heightScreen-heightHeader });
  $("#hero .hero.main").css({ "top": (heightScreen/2)-(heightHeroMain/2) });

  $("#speakers-page .single .modal").each(function() {
    var heightModal = $(this).outerHeight();
    var heightScreenModal = $(window).height();
    var heightScreenModalHead = heightScreenModal-60;
    var widthScreenModal = $(window).width();

    if (heightModal > heightScreenModalHead) {
      $(this).css({ "height": heightModal });
      $(this).css({ "overflow": "scroll" });
      $(this).css({ "padding-bottom": "150px" });
    } else {
      $(this).css({ "height": "auto" });
      $(this).css({ "overflow": "auto" });
      $(this).css({ "padding-bottom": "0" });
    }
    if (widthScreen < 680) {
      $(this).css({ "height": heightScreenModal-50 });
      $(this).css({ "overflow": "scroll" });
      $(this).css({ "padding-bottom": "0" });
    }
  });

  $('.square').each(function() {
    var width_square = $(this).outerWidth();
    $(this).css({ "height": width_square });
  });

  if (widthMap < 1200) {
    $(".container-map").css({ "height": mapRatio });
    $("#map").css({ "height": mapRatio });
  }
}

$(document).ready(function() {
  calcSize();

  paceOptions = {
    ajax: false, // disabled
    document: false, // disabled
    eventLag: false, // disabled
    elements: {
      selectors: ['body']
    }
  };

  $("header.header").sticky({topSpacing:0, className:"head-sticky"});
  $(".container-side-fixed").sticky({topSpacing:60});


  $(".target").roller({
    infinite: false,
    pagination: false,
    touchPaged: true
  });
  
  $('[data-typer-targets]').typer({
    highlightSpeed    : 500,
    typeSpeed         : 1000,
    clearDelay        : 600,
    typeDelay         : 1200,
    clearOnHighlight  : true,
    typerInterval     : 3000
  });

  $('.menu-primary-nav-container ul li a').on('click', function() {
    $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - 60}, 1000);
    return false;
  });

  $('#about .people-container .people a').on('click', function() {
    $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - 60}, 1000);
    return false;
  });

  $('.icon-search.search').click(function() {
    if ($('#undefined-sticky-wrapper').hasClass('head-sticky')) {
      $('.search-form').addClass("sticky");
    } else {
      $('.search-form').removeClass("sticky");
    }
    $('.search-form').addClass("active");
    $('.search-mobile').addClass("active");
    setTimeout(function() {
      $('#s').focus();
    }, 300);
  });

  $('.close-search-form').click(function() {
    $('.search-form').removeClass("active");
    $('.search-mobile').removeClass("active");
    setTimeout(function() {
      $('.search-form input[type="text"]').val("");
      $('#s').attr("placeholder", "Search Here");
    }, 300);
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      $('.search-form').removeClass("active");
      setTimeout(function() {
        $('.search-form input[type="text"]').val(""); 
        $('#s').attr("placeholder", "Search Here");
      }, 300);
     }
  });

  $('#socializzami').share({
    networks: ['facebook','twitter','googleplus']
  });

  $('.scroll-js').click(function() {
    $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - 60 }, 1000);
    return false;
  });

  $('#searchform').submit(function(e) { // run the submit function, pin an event to it
    var s = $( this ).find("#s"); // find the #s, which is the search input id
    if (!s.val()) { // if s has no value, proceed
      e.preventDefault(); // prevent the default submission
      $('#s').attr("placeholder", "Your search is empty");
      $('#s').focus(); // focus on the search input
    }
  });


  $("#map .container-map .single-pin .orange-pin").click(function(){
    $("#map .container-map .single-pin .orange-pin").removeClass("active");
    $(this).toggleClass("active");
  });

  // $('.page-filosofia ul li a').on('click', function() {
  //   $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - 70}, 1000);
  //   return false;
  // });
  
  $('.map .single-event').click(function() {
    $('.orange-pin').removeClass('active');
    //console.log('#' + $(this).data('hover'));
    var selector = '#' + $(this).data('hover');
    $(selector).find('.orange-pin').toggleClass('active');
  });

  $('#map .container-map .single-pin .orange-pin .close-event').click(function(e) {
    $('#map .container-map .single-pin .orange-pin').removeClass('active');
    e.stopPropagation();
  });

  $('#contact .container-form div.wpcf7-mail-sent-ok').click(function(e) {
    console.log("yo");
    $(this).addClass('hide');
    e.stopPropagation();
  })

  $('#submit').click(function() {
    setTimeout(function() {
      $('#contact .container-form div.wpcf7-mail-sent-ok').addClass('hide');
    }, 3000);
  });


  // Speakers Page
  if(window.location.hash !='') {
    $(window.location.hash).addClass('open');
    $('.overlay-modal').addClass('open');
  }

  $('#speakers-page .single').click(function() {
    $('#speakers-page .single').removeClass('open');
    $('.overlay-modal').addClass('open');
    $(this).addClass('open');
    var singleSpeaker = '#' + $(this).data('single');
    window.location.hash = singleSpeaker;
  });

  $('#speakers-page .single .close-modal').click(function(e) {
    $('#speakers-page .single').removeClass('open');
    $('.overlay-modal').removeClass('open');
    e.stopPropagation();
  });

  $('#speakers-page .single .modal .top .arrow-left').click(function(e) {
    var singleSpeaker = '#' + $(this).parents('.single').data('single');
    var singleId = '#' + $(this).parents('.single').prev().data('single');
    $(singleSpeaker).addClass('go-left');
    $('#speakers-page .single').removeClass('open');
    $(singleId).addClass('open');
    setTimeout(function() {
      $('#speakers-page .single').removeClass('go-left');
    }, 700);
    window.location.hash = singleId;
    e.stopPropagation();
  });

  $('#speakers-page .single .modal .top .arrow-right').click(function(e) {
    var singleSpeaker = '#' + $(this).parents('.single').data('single');
    var singleId = '#' + $(this).parents('.single').next().data('single');
    $(singleSpeaker).addClass('go-right');
    $('#speakers-page .single').removeClass('open');
    $(singleId).addClass('open');
    setTimeout(function() {
      $('#speakers-page .single').removeClass('go-right');
    }, 700);
    window.location.hash = singleId;
    e.stopPropagation();
  });

  // Team Page
  if(window.location.hash !='') {
    $(window.location.hash).addClass('open');
    $('.overlay-modal').addClass('open');
  }

  $('#team-page .single').click(function() {
    $('#team-page .single').removeClass('open');
    $('.overlay-modal').addClass('open');
    $(this).addClass('open');
    var singleSpeaker = '#' + $(this).data('single');
    window.location.hash = singleSpeaker;
  });

  $('#team-page .single .close-modal').click(function(e) {
    $('#team-page .single').removeClass('open');
    $('.overlay-modal').removeClass('open');
    e.stopPropagation();
  });

  $('#team-page .single .modal .top .arrow-left').click(function(e) {
    var singleSpeaker = '#' + $(this).parents('.single').data('single');
    var singleId = '#' + $(this).parents('.single').prev().data('single');
    $(singleSpeaker).addClass('go-left');
    $('#team-page .single').removeClass('open');
    $(singleId).addClass('open');
    setTimeout(function() {
      $('#team-page .single').removeClass('go-left');
    }, 700);
    window.location.hash = singleId;
    e.stopPropagation();
  });

  $('#team-page .single .modal .top .arrow-right').click(function(e) {
    var singleSpeaker = '#' + $(this).parents('.single').data('single');
    var singleId = '#' + $(this).parents('.single').next().data('single');
    $(singleSpeaker).addClass('go-right');
    $('#team-page .single').removeClass('open');
    $(singleId).addClass('open');
    setTimeout(function() {
      $('#team-page .single').removeClass('go-right');
    }, 700);
    window.location.hash = singleId;
    e.stopPropagation();
  });

  /* mobile nav */
    $(".open-nav").click(function(){
      $('.mega-container-body').toggleClass("nav-mobile-active");
      $('.header-mobile').toggleClass("nav-mobile-active");
      $('nav.mobile').toggleClass("nav-mobile-active");
      $('.search-mobile').toggleClass("nav-mobile-active");
      $('.single-blog .container-article article .gradient.fixed').toggleClass("nav-mobile-active");
    });
  
});

$(window).resize(function() {
  calcSize();
});