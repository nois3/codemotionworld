<?php get_header(); ?>
<?php wp_reset_postdata(); ?>

<?php get_template_part('partials/internal', 'sticky'); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!-- / NEWS / -->
<section id="article" class="article">
  <div class="row collapse">
    <div class="image-cover">
      <div class="image-internal"><?php the_post_thumbnail(); ?></div>
      <div class="background-opacity"></div>
      <div class="background-gradient"></div>
      <div class="main-container">
        <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
          <div class="title-container">
            <h2 class="title"><?php the_title(); ?></h2>
            <?php $subtitle = get_field('subtitle'); ?>
            <?php if($subtitle) { ?>
              <h3 class="subtitle"><?php the_field('subtitle'); ?></h3>
            <?php } ?>
            <span class="double-line big"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row article-background collapse">
    <div class="large-8 medium-10 columns medium-centered large-centered clearfix">
      <div class="single-new">
        <div class="article-container">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php endwhile; /* fine del loop */ ?>

<?php get_footer(); ?>