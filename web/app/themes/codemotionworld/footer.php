<!-- / FOOTER / -->
<footer class="clearfix">
  <div class="row">
    <div class="large-12 medium-12 columns large-centered medium-centered">
      <?php the_field('footer_text', 'option'); ?>
      <br>
      <a href="http://nois3.it" title="we are nois3 | digital design thinking" class="orange" target="_blank">Designed with <span class="icon-heart"></span> by nois3.it</a>
    </div>
  </div>
</footer>

<?php get_search_form(); ?>
</div>
<?php wp_footer(); ?>
</body>
</html>